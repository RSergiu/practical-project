package model;

import dao.MovieDao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table (name = "theater")
public class Theater {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column (name = "id")
    private int id;

    @Column (name = "location")
    private String location;

    @Column (name = "rating")
    private double rating;

    @Column (name = "adress")
    private String adress;

    @ManyToMany (mappedBy = "theaters")
    private List<Movie> movies = new ArrayList<Movie>();

    @OneToMany (mappedBy = "theater")
    private Set<Facilities> facilitiesList = new HashSet<Facilities>();


    public Theater(String location, double rating, String adress) {
        this.location = location;
        this.rating = rating;
        this.adress = adress;
    }

    public Theater() {
    }

    public String getAdress() {
        return adress;
    }

    public Theater setAdress(String adress) {
        this.adress = adress;
        return this;
    }

    public double getRating() {
        return rating;
    }

    public Theater setRating(double rating) {
        this.rating = rating;
        return this;
    }

    public int getId() {
        return id;
    }

    public Theater setId(int id) {
        this.id = id;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public Theater setLocation(String location) {
        this.location = location;
        return this;
    }
}

package model;

import dao.BookingDao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column (name = "id")
    private int id;

    @Column (name = "number")
    private int number;

    @Column (name = "seats")
    private int seats;

    @Column (name = "details")
    private String details;

    @ManyToOne
    @JoinColumn (name = "movie_id",nullable = false)
    private Movie movie;

    @OneToOne (mappedBy = "booking")
    private User user;

    public Booking(int number, int seats, String details,Movie movie) {
        this.number = number;
        this.seats = seats;
        this.details = details;
        this.movie = movie;
    }

    public Booking() {

    }

    public User getUser() {
        return user;
    }

    public Booking setUser(User user) {
        this.user = user;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public Booking setNumber(int number) {
        this.number = number;
        return this;
    }

    public int getSeats() {
        return seats;
    }

    public Booking setSeats(int seats) {
        this.seats = seats;
        return this;
    }

    public String getDetails() {
        return details;
    }

    public Booking setDetails(String details) {
        this.details = details;
        return this;
    }

}

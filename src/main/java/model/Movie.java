package model;

import dao.MovieDao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table (name = "movie")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int id;

    @Column (name = "movieName")
    private String movieName;

    @Column (name = "genre")
    private String genre;


    @ManyToMany (cascade ={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.EAGER)
    @JoinTable (name = "movie_theater",
                joinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn (name = "theater_id", referencedColumnName = "id")
    )
    private List<Theater> theaters = new ArrayList<Theater>();

    @OneToMany (mappedBy = "movie")
    private Set<Booking> booking = new HashSet<Booking>();

    public Movie(String movieName, String genre) {
        this.movieName = movieName;
        this.genre = genre;
    }

    public Movie() {

    }

    public int getId() {
        return id;
    }

    public Movie setId(int id) {
        this.id = id;
        return this;
    }

    public String getMovieName() {
        return movieName;
    }

    public Movie setMovieName(String movieName) {
        this.movieName = movieName;
        return this;
    }

    public String getGenre() {
        return genre;
    }

    public Movie setGenre(String genre) {
        this.genre = genre;
        return this;
    }

    public List<Theater> getTheaters() {
        return theaters;
    }

    public Movie setTheaters(List<Theater> theaters) {
        this.theaters = theaters;
        return this;
    }
}

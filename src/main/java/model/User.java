package model;

import dao.UserDao;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column (name = "id")
    private int id;

    @Column (name = "username")
    private String username;

    @Column (name = "email")
    private String email;

    @Column (name = "phoneNumber")
    private String phoneNumber;

    @OneToOne
    @JoinColumn (name = "booking_id")
    private Booking booking;

    public User(String username, String email, String phoneNumber,Booking booking) {
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.booking = booking;
    }

    public User() {

    }

    public Booking getBooking() {
        return booking;
    }

    public User setBooking(Booking booking) {
        this.booking = booking;
        return this;
    }

    public int getId() {
        return id;
    }

    public User setId(int id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public User setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
}

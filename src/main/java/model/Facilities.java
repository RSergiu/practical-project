package model;

import dao.FacilitiesDao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table (name = "facilities")
public class Facilities{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column (name = "id")
    private int id;

    @Column (name = "type")
    private String type;

    @ManyToOne
    @JoinColumn (name = "theater_id",nullable = false)
    private Theater theater;

    public Facilities(String type,Theater theater) {
        this.type = type;
        this.theater = theater;
    }

    public Facilities() {

    }

    public Theater getTheater() {
        return theater;
    }

    public Facilities setTheater(Theater theater) {
        this.theater = theater;
        return this;
    }

    public String getType() {
        return type;
    }

    public Facilities setType(String type) {
        this.type = type;
        return this;
    }
}


package dao;

import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class UserDaoImpl implements UserDao {

    SessionFactory sessionFactory;

    public UserDaoImpl() {
        sessionFactory = Utils.getSessionFactory();
    }

    public User createUser(User user) {
        Session session = this.sessionFactory.openSession();
        session.save(user);
        session.close();
        return user;
    }

    public void deleteUser(User user) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(user);
        transaction.commit();
        session.close();
    }

    public User getUser(int id) {
        Session session = this.sessionFactory.openSession();
        User user = session.get(User.class,id);
        session.close();
        return user;
    }

    public User updateUser(User user,int id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        User user1 = session.get(User.class,id);
        user1.setUsername(user.getUsername());
        user1.setEmail(user.getEmail());
        user1.setPhoneNumber(user.getPhoneNumber());
        user1.setBooking(user.getBooking());
        session.update(user1);
        transaction.commit();
        session.close();
        return user;
    }

    public void deleteUserById(int id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        User user = session.get(User.class,id);
        session.remove(user);
        transaction.commit();
        session.close();
    }
}

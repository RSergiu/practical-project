package dao;

import model.Booking;
import model.Facilities;
import model.Theater;
import model.Movie;
import model.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class Utils {

    private static SessionFactory sessionFactory = createConfiguration().buildSessionFactory();

    private Utils() {

    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Configuration createConfiguration() {
        Configuration config = new Configuration();

        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
        settings.put(Environment.URL, "jdbc:mysql://localhost:3306/practicalproject?serverTimezone=UTC");
        settings.put(Environment.USER, "rusdsergiu");
        settings.put(Environment.PASS, "Sergiur123");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        config.setProperties(settings);
        config.addAnnotatedClass(Facilities.class);
        config.addAnnotatedClass(Booking.class);
        config.addAnnotatedClass(Theater.class);
        config.addAnnotatedClass(Movie.class);
        config.addAnnotatedClass(User.class);
        return config;
    }
}

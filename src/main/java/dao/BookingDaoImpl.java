package dao;

import model.Booking;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class BookingDaoImpl implements BookingDao {

    SessionFactory sessionFactory;

    public BookingDaoImpl() {
        sessionFactory = Utils.getSessionFactory();
    }

    public Booking createBooking(Booking booking) {
        Session session = this.sessionFactory.openSession();
        session.save(booking);
        session.close();
        return booking;
    }

    public void deleteBooking(Booking booking) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(booking);
        transaction.commit();
        session.close();
    }

    public Booking getBooking(int id) {
        Session session = this.sessionFactory.openSession();
        Booking booking = session.get(Booking.class,id);
        session.close();
        return booking;
    }

    public Booking updateBooking(Booking booking,int id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Booking booking1 = session.get(Booking.class,id);
        booking1.setNumber(booking.getNumber());
        booking1.setSeats(booking.getSeats());
        booking1.setDetails(booking.getDetails());
        session.update(booking1);
        transaction.commit();
        session.close();
        return booking;
    }
}

package dao;

import model.Booking;
import model.Facilities;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class FacilitiesDaoImpl implements FacilitiesDao {

    SessionFactory sessionFactory;

    public FacilitiesDaoImpl() {
        sessionFactory = Utils.getSessionFactory();
    }

    public Facilities createFacilities(Facilities facilities) {
        Session session = this.sessionFactory.openSession();
        session.save(facilities);
        session.close();
        return facilities;
    }

    public void deleteFacilities(Facilities facilities) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(facilities);
        transaction.commit();
        session.close();
    }

    public Facilities getFacilities(int id) {
        Session session = this.sessionFactory.openSession();
        Facilities facilities = session.get(Facilities.class,id);
        session.close();
        return facilities;
    }

    public Facilities updateFacilities(Facilities facilities,int id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Facilities facilities1 = session.get(Facilities.class,id);
        facilities1.setTheater(facilities.getTheater());
        facilities1.setType(facilities.getType());
        session.update(facilities1);
        transaction.commit();
        session.close();
        return facilities;
    }
}

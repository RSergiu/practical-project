package dao;

import model.Facilities;

public interface FacilitiesDao {

    Facilities createFacilities(Facilities facilities);
    void deleteFacilities(Facilities facilities);
    Facilities getFacilities(int id);
    Facilities updateFacilities(Facilities facilities,int id);
}

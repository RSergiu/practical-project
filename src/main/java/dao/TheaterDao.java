package dao;

import model.Theater;

public interface TheaterDao {

    Theater createTheater(Theater theater);
    void deleteTheater(Theater theater);
    Theater getTheater(int id);
    Theater getTheaterByName(String name);
    Theater updateTheater(Theater theater,int id);
}

package dao;

import model.Movie;
import model.Theater;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class TheaterDaoImpl implements TheaterDao {

    SessionFactory sessionFactory;

    public TheaterDaoImpl() {
        sessionFactory = Utils.getSessionFactory();
    }

    public Theater createTheater(Theater theater) {
        Session session = this.sessionFactory.openSession();
        session.save(theater);
        session.close();
        return theater;
    }

    public void deleteTheater(Theater theater) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(theater);
        transaction.commit();
        session.close();
    }

    public Theater getTheater(int id) {
        Session session = this.sessionFactory.openSession();
        Theater theater = session.get(Theater.class, id);
        session.close();
        return theater;
    }

    public Theater getTheaterByName(String name) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Theater theater = session.get(Theater.class,name);
        theater.getLocation();
        transaction.commit();
        session.close();
        return theater;
    }

    public List<Theater> getAllTheaters() {
        Session session = this.sessionFactory.openSession();
        Query query = session.createQuery("from theater");
        List<Theater> theaters = query.list();
        return theaters;
    }

    public Theater updateTheater(Theater theater,int id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Theater theater1 = session.get(Theater.class, id);
        theater1.setLocation(theater.getLocation());
        theater1.setAdress(theater.getAdress());
        theater1.setRating(theater.getRating());
        session.update(theater1);
        transaction.commit();
        session.close();
        return theater;
    }
}

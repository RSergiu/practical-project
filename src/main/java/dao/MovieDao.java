package dao;

import model.Movie;

public interface MovieDao {

    Movie createMovie(Movie movie);
    void deleteMovie(Movie movie);
    Movie getMovie(int id);
    Movie update (Movie movie,int id);
    void deleteMovieById(int id);
}

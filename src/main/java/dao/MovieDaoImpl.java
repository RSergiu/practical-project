package dao;

import model.Movie;
import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.ManyToOne;

public class MovieDaoImpl implements MovieDao {

    SessionFactory sessionFactory;

    public MovieDaoImpl() {
        sessionFactory = Utils.getSessionFactory();
    }

    public Movie createMovie(Movie movie) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(movie);
        transaction.commit();
        session.close();
        return movie;
    }

    public void deleteMovie(Movie movie) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(movie);
        transaction.commit();
        session.close();
    }

    public Movie getMovie(int id) {
        Session session = this.sessionFactory.openSession();
        Movie movie = session.get(Movie.class, id);
        session.close();
        return movie;
    }

    public Movie update(Movie movie,int id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Movie movie1 = session.get(Movie.class,id);
        movie1.setMovieName(movie.getMovieName());
        movie1.setGenre(movie.getGenre());
        session.update(movie1);
        transaction.commit();
        session.close();
        return movie;
    }

    public void deleteMovieById(int id) {
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Movie movie = session.get(Movie.class,id);
        session.remove(movie);
        transaction.commit();
        session.close();
    }
}

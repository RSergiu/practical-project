package dao;

import model.Booking;

public interface BookingDao {

    Booking createBooking(Booking booking);
    void deleteBooking(Booking booking);
    Booking getBooking(int id);
    Booking updateBooking(Booking booking,int id);
}

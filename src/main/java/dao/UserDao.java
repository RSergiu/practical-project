package dao;

import model.User;


public interface UserDao {

    User createUser(User user);
    void deleteUser(User user);
    void deleteUserById(int id);
    User getUser(int id);
    User updateUser(User user,int id);
}

package javafx;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.hibernate.boot.jaxb.internal.stax.HbmEventReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class CinemaApp extends Application {

    Stage window;
    Scene cityScene, cinemaScene, movieScene, timeScene, placeScene, userScene;

    public static void main(String[] args) {
        launch();
    }

    public void init() {
        System.out.println("App start.");
    }

    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setHeight(700);
        window.setWidth(900);
        window.setX(0);
        window.setY(0);
        window.setTitle("CinemaApplication");

        Text chooseText = new Text("Choose a city:");
        Text chooseText1 = new Text("Choose a cinema:");
        Text chooseText2 = new Text("Choose a movie:");
        Text chooseText3 = new Text("Insert date:");
        Text chooseText4 = new Text("Select time:");
        Text chooseText5 = new Text("Choose your seat:");
        Text chooseText6 = new Text("Enter your info:");
        Button button = new Button("Submit");
        button.setOnMouseClicked(event -> window.setScene(cinemaScene));



        VBox layoutCity = new VBox();
        layoutCity.getChildren().add(chooseText);
        layoutCity.setSpacing(10);

        chooseText.setTextAlignment(TextAlignment.JUSTIFY);
        cityScene = new Scene(layoutCity);

        ComboBox<String> citiesComboBox = new ComboBox<String>();
        citiesComboBox.getItems().addAll("Cluj-Napoca", "Oradea", "Bucuresti", "Brasov", "Timisoara");
        citiesComboBox.getSelectionModel().select(1);
        layoutCity.setAlignment(Pos.CENTER);
        layoutCity.getChildren().addAll(citiesComboBox,button);


        VBox layoutCinema = new VBox();
        layoutCinema.setAlignment(Pos.CENTER);
        layoutCinema.setSpacing(10);

        ComboBox<String> cinemaComboBox = new ComboBox<>();
        cinemaComboBox.getItems().addAll("Cinema Vivo","Cinema Iulius", "Florin Piersic", "Cinema Victoria", "Cinema Arta");
        cinemaComboBox.getSelectionModel().select(1);


        Button button1 = new Button("Submit");
        Button backButton1 = new Button("Back");
        layoutCinema.getChildren().addAll(chooseText1,cinemaComboBox,button1,backButton1);
        cinemaScene = new Scene(layoutCinema);

        button1.setOnMouseClicked(event -> window.setScene(movieScene));
        backButton1.setOnMouseClicked(event -> window.setScene(cityScene));

        VBox layoutMovie = new VBox();
        layoutMovie.setAlignment(Pos.CENTER);
        layoutMovie.setSpacing(10);

        ComboBox<String> movieComboBox = new ComboBox<>();
        movieComboBox.getItems().addAll("Horror","Action","Commedy","Thriller","SciFi");
        movieComboBox.getSelectionModel().select(1);

        Button button2 = new Button("Submit");
        Button backButton2 = new Button("Back");
        layoutMovie.getChildren().addAll(chooseText2,movieComboBox,button2,backButton2);
        movieScene = new Scene(layoutMovie);

        button2.setOnMouseClicked(event -> window.setScene(timeScene));
        backButton2.setOnMouseClicked(event -> window.setScene(cinemaScene));


        VBox layoutTimeAndDate = new VBox();
        layoutTimeAndDate.setAlignment(Pos.CENTER);
        layoutTimeAndDate.setSpacing(10);
        TextField dateText = new TextField();
        dateText.setMaxSize(100,100);


        Button button3 = new Button("Validate");
        Button button4 = new Button("Submit");
        Button backButton3 = new Button("Back");
        button3.setOnMouseClicked(event -> System.out.println(dateText.getText()));
        button4.setOnMouseClicked(event -> window.setScene(userScene));
        backButton3.setOnMouseClicked(event -> window.setScene(movieScene));

        ComboBox<String> timeComboBox = new ComboBox<>();
        timeComboBox.getSelectionModel().select(1);
        timeComboBox.getItems().addAll("12:00","15:00","17:25","19:10","21:45");
        layoutTimeAndDate.getChildren().addAll(chooseText3,dateText,chooseText4,button3,timeComboBox,button4,backButton3);
        timeScene = new Scene(layoutTimeAndDate);


        VBox layoutUser = new VBox();
        layoutUser.setAlignment(Pos.CENTER);
        layoutUser.setSpacing(10);
        TextField userName = new TextField();
        TextField userEmail = new TextField();
        TextField userPhoneNumber = new TextField();
        userName.setMaxSize(150,100);
        userEmail.setMaxSize(150,100);
        userPhoneNumber.setMaxSize(150,100);

        Button button5 = new Button("Make reservation");
        Button backButton4 = new Button("Back");
        button5.setOnMouseClicked(event -> System.out.println(userName.getText() + " " + userEmail.getText() + " " + userPhoneNumber.getText()));
        backButton4.setOnMouseClicked(event -> window.setScene(timeScene));

        layoutUser.getChildren().addAll(userName,userEmail,userPhoneNumber,button5,backButton4);
        userScene = new Scene(layoutUser);



        FileInputStream imageInput = new FileInputStream("/Users/rusdsergiu/practical-project/shutterstock_384996514.jpg");

        Image image = new Image(imageInput);

        final BackgroundSize backgroundSize = new BackgroundSize(primaryStage.getWidth(),primaryStage.getHeight(),false,false,false,false);
        BackgroundImage backgroundImage = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                backgroundSize);

        Background background = new Background(backgroundImage);
        layoutCity.setBackground(background);
        layoutCinema.setBackground(background);
        layoutMovie.setBackground(background);
        layoutTimeAndDate.setBackground(background);
        layoutUser.setBackground(background);

        window.setScene(cityScene);
        window.show();
    }


    public void stop() {
        System.out.println("App stop.");
    }
}

package service;

import model.Theater;

import java.util.List;

public interface CinemaAppService {

    public List<Theater> getAllTheaters();
}
